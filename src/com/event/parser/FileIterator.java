package com.event.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class FileIterator {
	private static ArrayList<String> filelist = new ArrayList<String>(); 

    public static List<String> iterator(String strPath) { 
        File dir = new File(strPath); 
        File[] files = dir.listFiles(); 
        
        if (files == null) 
            return filelist; 
        for (int i = 0; i < files.length; i++) { 
            if (files[i].isDirectory()) { 
            	iterator(files[i].getAbsolutePath()); 
            } else if(files[i].getName().endsWith(".js")){ 
                filelist.add(files[i].getAbsolutePath());                    
            } 
        } 
        return filelist;
    }
}
