package com.event.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EventParser {
	Pattern triggerReg;
	Pattern onReg;
	List<Map<String, String>> relations = new ArrayList<Map<String, String>>();
	Map<String, List<String>> triggerMap = new HashMap<String, List<String>>();
	Map<String, List<String>> onMap = new HashMap<String, List<String>>();
	Map<String, String> relation = new HashMap<String, String>();
	Matcher matcher;
	String basePath;
	
	public void parser(String path){
		this.basePath = path;
		List<String> fileList = FileIterator.iterator(path);
		setReg();
		for(String fileName: fileList){
			parserEvent(fileName);
		}
		linkRelation();
		DataCreater.create(relations);
		System.out.println(relations);
	}
	public void setReg(){
		String[] config = FileContentReader.read("config/reg.txt").split("\n");
		triggerReg = Pattern.compile(config[0]);
		onReg = Pattern.compile(config[1]);
	}
	public void parserEvent(String path){
		String content = FileContentReader.read(path);
		path = path.replace(basePath, "");
		String eventName = "";
		matcher = triggerReg.matcher(content);
		// 统计发布事件
		while(matcher.find()){
			eventName = matcher.group();
			if(triggerMap.get(eventName) != null){
				triggerMap.get(eventName).add(path);
			}
			else{
				List<String> list = new ArrayList<String>();
				list.add(path);
				triggerMap.put(eventName, list);
			}
		}
		// 统计接受事件
		matcher = onReg.matcher(content);
		while(matcher.find()){
			eventName = matcher.group();
			if(onMap.get(eventName) != null){
				onMap.get(eventName).add(path);
			}
			else{
				List<String> list = new ArrayList<String>();
				list.add(path);
				onMap.put(eventName, list);
			}
		}
	}
	public void linkRelation(){
		Set<String> key = triggerMap.keySet();
        for (Iterator it = key.iterator(); it.hasNext();) {
            String s = (String) it.next();
            if(onMap.get(s) != null){
            	for(String source : triggerMap.get(s)){
            		for(String target : onMap.get(s)){
            			relation = new HashMap<String, String>();
    					relation.put("source", source.replace("\\", "/"));
    					relation.put("target", target.replace("\\", "/"));
    					relation.put("title", s.replace("\\", "/"));
    					relations.add(relation);
                	}
            	}
            }
        }
	}
}
