package com.event.parser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.List;
import java.util.Map;

public class DataCreater {
	public static void create(List<Map<String, String>> relations){
		StringBuffer content = new StringBuffer("var data = [");
		for(Map<String, String> m : relations){
			content.append("{");
			content.append("source: '" + m.get("source") + "',");
			content.append("target: '" + m.get("target") + "',");
			content.append("title: '" + m.get("title") + "'");
			content.append("},\n");
		}
		content.append("];");
		File f = new File("demo/data.js");
		try{
		     BufferedWriter writer = new BufferedWriter(new FileWriter(f));
		     writer.write(content.toString());
		     writer.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
