package com.event.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;

public class FileContentReader {
	public static String read(String path){
		StringBuffer content = new StringBuffer();
		String s;  
		BufferedReader in = null;
        try {
        	in = new BufferedReader(new InputStreamReader(new FileInputStream(new File(path))));
			while ((s = in.readLine()) != null){
				content.append(s + "\n");
			}
			    
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        finally{
        	try {
        		if(in != null){
        			in.close();
        		}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
        }
		return content.toString();
	}
}
